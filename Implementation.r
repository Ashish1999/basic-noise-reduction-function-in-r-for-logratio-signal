#Installing and importing the required packages
#Uncomment the below lines to install the required packages
#install.packages(c('lattice','IDPmisc','pracma','ggplot2')) 
library(lattice)
library(IDPmisc)
library(pracma)
library(ggplot2)
library(grid)

#Changing the Working directory to the location of the tsv file
setwd("/home/ashish/Documents/scones/data_test")

# Reading the data
data = read.csv(file = "SCONES_test.tsv", check.names = FALSE,sep="\t",header=TRUE) 

#Checking for Infinte values in data
unique(is.infinite(data$testSample1))
unique(is.infinite(data$testSample2))


#Now we will remove the Infinite values & check again in case they were not removed
data <- NaRV.omit(data)               
unique(is.infinite(data$testSample1))      
unique(is.infinite(data$testSample2))      

#Change chr to factor & get summary of the data
data$chr <- as.factor(data$chr)       
str(data)                   
#attach(data) 

# Differentiate the data with respect to chr
chr1.data <- subset(data, data$chr == 1)
chr2.data <- subset(data, data$chr == 2)

#Get the number of rows
row = nrow(data)             

# Plot the data for different level
par(mfrow=c(2,2))
ggplot(data = data, aes(x= 1:15486, y = testSample1, color = factor(chr))) + geom_point() + scale_color_discrete(labels = c("1", "2")) + labs(color = "chr value", y = "Test Sample 1 Values")
ggplot(data = data, aes(x= 1:15486, y = testSample2, color = factor(chr))) + geom_point() + scale_color_discrete(labels = c("1", "2")) + labs(color = "chr value", y = "Test Sample 2 Values")

# Box Plot of test Sample 1 & 2
par(mfrow=c(1,2))
boxplot(data$testSample1, main="Test Sample 1")
boxplot(data$testSample2, main="Test Sample 2")

# logRatio signal
logratio.data <- cbind.data.frame(data$chr, logratio=log(abs(data$testSample1/data$testSample2), base = 2))
var(logratio.data$logratio)


#The Filter to reduce noise as we are not aiming towards removing noise

logratio.filter <- movavg(logratio.data$logratio, 10, type="m")


logratio.filter.data <- cbind.data.frame(data$chr, logratio.filter)
var(logratio.filter.data$logratio.filter)

#Plot of logratio data before noise reduction
ggplot(data = logratio.data, aes(x= 1:15486, y = logratio, color = factor(data$chr))) + geom_point() + scale_color_discrete(labels = c("1", "2")) + labs(color = "chr value", y = "LogRatio Signal")

#Plot of logratio data after noise reduction
ggplot(data = logratio.filter.data, aes(x= 1:15486, y = logratio.filter, color = factor(data$chr))) + geom_point() + scale_color_discrete(labels = c("1", "2")) + labs(color = "chr value", y = "Filtered LogRatio Signal")
