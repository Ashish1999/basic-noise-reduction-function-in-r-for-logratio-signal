#Installing and importing the required packages
#Uncomment the below lines to install the required packages
#install.packages(c('IDPmisc','pracma')) 
library(IDPmisc)
library(pracma)

#Changing the Working directory to the location of the tsv file
setwd("/home/ashish/Documents/scones/data_test")

# Reading the data
test = read.csv(file = "SCONES_test.tsv", check.names = FALSE,sep="\t",header=TRUE) 


noisereduction <- function(data) {
	#Checking for Infinte values in data
	unique(is.infinite(data$testSample1))      
	unique(is.infinite(data$testSample2))      

	#Now we will remove the Infinite values
	data <- NaRV.omit(data)               
     
	#Change chr to factor 
	data$chr <- as.factor(data$chr)       

	#Differentiate the data with respect to chr
	chr1.data <- subset(data, data$chr == 1)
	chr2.data <- subset(data, data$chr == 2)

	logratio.data <- cbind.data.frame(data$chr, logratio=log(abs(data$testSample1/data$testSample2), base = 2))         # logRatio signal
	var(logratio.data$logratio)

	#The Filter to reduce noise as we are not aiming towards removing noise
	logratio.filter <- movavg(logratio.data$logratio, 10, type="m")
	logratio.filter.data <- cbind.data.frame(data$chr, logratio.filter)
	var(logratio.filter.data$logratio.filter)

	return(logratio.filter.data)
}

noisereduction(test)
